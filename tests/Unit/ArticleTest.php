<?php

namespace Tests\Unit;

use App\Article;
use App\Http\Resources\ArticleWithComments;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Http\Resources\Article as ArticleResource;
use Faker\Generator as Faker;

class ArticleTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function get_all_articles()
    {
        $this->json('GET', route('api.articles'))
            ->assertStatus(200);
    }

    /** @test */
    function show_article()
    {
        $article = factory(Article::class)->create();
        $resource = ArticleWithComments::make($article)->response()->getData(true);
        $response = $this->json('GET', route('api.articles.show', $article))->json();
        $this->assertEquals($response, $resource);
    }

    /** @test */
    function search_article()
    {
        $article = factory(Article::class)->create();
        $response = $this->json('GET', route('api.articles', ['q' => $article->title]))->assertStatus(200)->json('data');
        $this->assertEquals(count($response), 1);
    }

    /** @test */
    function check_create_article_validation()
    {
        $this->json('POST', route('api.articles.create'))
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'title' => [
                        'The title field is required.'
                    ],
                    'body' => [
                        'The body field is required.'
                    ]
                ]
            ]);
    }

    /** @test */
    function create_article()
    {
        $title = 'My title';
        $response = $this->post(route('api.articles.create'), ['title' => $title, 'body' => 'Body of article'])
            ->assertStatus(201)->json();
        $this->assertEquals($title, $response['data']['title']);
    }

    /** @test */
    function update_article()
    {
        $article = factory(Article::class)->create();
        $response = $this->json('PUT',
            route('api.articles.update', $article), ['title' => 'New title', 'body' => 'New body'])
            ->assertStatus(200)->json('data');
        $this->assertEquals($response['title'], 'New title');
    }


    /** @test */
    function delete_article()
    {
        $article = factory(Article::class)->create();
        $this->json('DELETE', route('api.articles.delete', $article->id))
            ->assertStatus(204);
        $this->json('GET', 'api/articles/'.$article->id)->assertStatus(404);
    }
}
