<?php

namespace Tests\Unit;

use App\Article;
use App\Comment;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Http\Resources\Comment as CommentResource;

class CommentTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function check_create_comment_validation()
    {
        $article = factory(Article::class)->create();
        $this->json('POST', route('api.comments.create', $article))
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'body' => [
                        'The body field is required.'
                    ],
                ]
            ]);
    }

    /** @test */
    function create_comment()
    {
        $article = factory(Article::class)->create();
        $this->post(route('api.comments.create', $article), ['body' => 'Body of article'])
            ->assertStatus(201);
    }

    /** @test */
    function delete_comment()
    {
        $comment = factory(Comment::class)->create();
        $this->json('DELETE', route('api.comments.delete',$comment))
            ->assertStatus(204);
    }

    /** @test */
    function update_comment()
    {
        $comment = factory(Comment::class)->create();
        $response = $this->json('PUT',
            route('api.comments.update', $comment), ['body' => 'New comment'])
            ->assertStatus(200)->json('data');
        $this->assertEquals($response['body'], 'New comment');
    }

}
