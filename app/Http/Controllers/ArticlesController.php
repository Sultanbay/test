<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Resources\ArticleWithComments;
use Illuminate\Http\Request;
use App\Http\Requests\Article as ArticleRequest;
use App\Http\Resources\Article as ArticleResource;
class ArticlesController extends Controller
{
    public function index()
    {
        if (\request('q'))
        {
            return ArticleResource::collection(Article::search(\request('q'))->get());
        }
        return ArticleResource::collection(Article::all());
    }

    public function create(ArticleRequest $request)
    {
        $article = Article::create([
            'title' => $request->title,
            'body' => $request->body,
            'author' => $request->ip()
        ]);

        return new ArticleResource($article);
    }

    public function show(Article $article)
    {
        return new ArticleWithComments($article);
    }

    public function update(ArticleRequest $request, Article $article)
    {
        $article->update($request->only(['title', 'body']));
        return new ArticleResource($article);
    }

    public function delete(Article $article)
    {
        $article->delete();
        return response()->json(null, 204);
    }
}
