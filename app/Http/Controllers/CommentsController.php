<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\Comment as CommentRequest;
use App\Http\Resources\Comment as CommentResource;

class CommentsController extends Controller
{
    public function create(CommentRequest $request, Article $article)
    {
        $comment = $article->comments()
            ->create(['body' => $request->body, 'author' => $request->ip()]);
        return new CommentResource($comment);
    }

    public function update(CommentRequest $request, Comment $comment)
    {
        $comment->update($request->only(['body']));
        return new CommentResource($comment);
    }

    public function delete(Comment $comment)
    {
        $comment->delete();
        return response()->json(null, 204);
    }
}
