<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'body', 'author'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public static function search(string $q)
    {
        return self::where('title', 'like', '%'.$q.'%')->orWhere('body', 'like', '%'.$q.'%');
    }

    public function getDateAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d');
    }
}
