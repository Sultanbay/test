<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'articles'], function() {
    Route::get('/', 'ArticlesController@index')->name('api.articles');
    Route::get('/{article}', 'ArticlesController@show')->name('api.articles.show');
    Route::post('/', 'ArticlesController@create')->name('api.articles.create');
    Route::put('/{article}', 'ArticlesController@update')->name('api.articles.update');
    Route::delete('/{article}', 'ArticlesController@delete')->name('api.articles.delete');

    Route::post('/{article}/comments', 'CommentsController@create')->name('api.comments.create');
});

Route::group(['prefix' => 'comments'], function() {
    Route::put('/{comment}', 'CommentsController@update')->name('api.comments.update');
    Route::delete('/{comment}', 'CommentsController@delete')->name('api.comments.delete');
});
